﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoneyTransfer.Models;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class GeneratedNumbersTests
    {
        [Fact]
        public void GeneratedNumberHasSixDigits()
        {
            GeneratedNumbers generatedNumbers = new GeneratedNumbers();
            int? result = Convert.ToInt32(generatedNumbers.GenerateNumber()) as int?;
            Assert.Equal(6, result?.ToString().Length);
        }

        //[Fact]
        //public void GeneratedNumberIsUnique()
        //{
        //    GeneratedNumbers generatedNumbers = new GeneratedNumbers();
        //    Random rnd = new Random();
        //    int uniqueNumber = rnd.Next(100000, 999999);
        //    GeneratedNumbers number = new GeneratedNumbers();
        //    number.GeneratedNumber = uniqueNumber;
        //    int? result = 
        //}
    }
}
