﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public int SumOfTransaction { get; set; }
        public DateTime DateOfTransaction { get; set; }
        public bool TransactionSuccess { get; set; }
    }
}
