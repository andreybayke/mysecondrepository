﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.Models
{
    public class GeneratedNumbers
    {
        public string GeneratedNumber { get; set; }

        public string GenerateNumber()
        {
            Random rnd = new Random();
            string uniqueNumber = rnd.Next(100000, 999999).ToString();
            GeneratedNumbers number = new GeneratedNumbers();
            number.GeneratedNumber = uniqueNumber;
            return uniqueNumber;
        }

        public bool CheckGeneratedNumber(string uniqueNumber)
        {
            List<User> allUsers = new List<User>();
            List<User> checkingList = allUsers.Where(g => g.UserName == uniqueNumber.ToString()).ToList();
            if (checkingList.Count != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public string CheckedGeneratedNumber()
        {
            string FinalGeneratedNumber = GenerateNumber();
            CheckGeneratedNumber(FinalGeneratedNumber);
            if (CheckGeneratedNumber(FinalGeneratedNumber))
            {
                return FinalGeneratedNumber;
            }
            else
            {
                return CheckedGeneratedNumber();
            }
        }
    }
}
