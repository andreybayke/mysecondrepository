﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoneyTransfer.Models;

namespace MoneyTransfer.Controllers
{
    public class TransactionController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly Transaction _transaction;
        private ApplicationContext _context;

        public TransactionController(UserManager<User> userManager, Transaction transaction,
            ApplicationContext context)
        {
            _userManager = userManager;
            _transaction = transaction;
            _context = context;
        }

        [Authorize]
        public async Task<IActionResult> Create()
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            ViewBag.UserId = user.Id;
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(Transaction transaction)
        {
            User user = await _userManager.FindByNameAsync(User.Identity.Name);
            if (ModelState.IsValid)
                {
                    if (transaction.SumOfTransaction < user.Balance)
                    {
                        transaction.DateOfTransaction = DateTime.Now;
                        _context.Transaction.Add(transaction);
                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index" , "User");
                    }
                    else
                    {
                    return RedirectToAction("Create", new { id = transaction.Id });
                }
                }
                else
                {
                    return RedirectToAction("Create");
                }
        }
    }
}